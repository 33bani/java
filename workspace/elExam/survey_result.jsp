<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<table>
	<tr>
		<td> 이름  : </td>
		<td>${param.name}</td>
	</tr>
	<tr>
		<td> 전화번호  : </td>
		<td>${param.number}</td>
	</tr>
	<tr>
		<td> 이메일  : </td>
		<td>${param.email}</td>
	</tr>
	<tr>
		<td> 알게된 경로  : </td>
		<td>${param.route}</td>
	</tr>
	<tr>
		<td> 좋아하는 스포츠  : </td>
		<td>${param.sports}</td>
	</tr>
	<tr>
		<td> 연락 방법  : </td>
		<td>${param.contact}</td>
	</tr>
</table>
</body>
</html>