<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	pageContext.setAttribute("p1", "page scope value");
	request.setAttribute("r1", "request scope value");
	session.setAttribute("s1", "session scope value");
	application.setAttribute("a1", "application scope value");
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	pageContext.setAttribute("p1") : ${pageScope.p1}<br>
	request.setAttribute("r1") : ${requestScope.r1}<br>
	session.setAttribute("p1") : ${s1}<br>
	application.setAttribute("r1") : ${a1}
</body>
</html>