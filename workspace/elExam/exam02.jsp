<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setAttribute("k", 10);
	request.setAttribute("m", true);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
k = ${k }<br>
k + 5 : ${k+5 }<br>
k - 10 : ${k-10 }<br>
k * 30 : ${k mod 30 }<br>
k/30 : ${k div 30 }<br>
<br>
m = ${m }<br>
k = ${k }<br>
k > 5 : ${ k > 5 } <br>
k < 5 : ${ k < 5 } <br>
k <= 10 : ${ k <= 10} <br>
k >= 10 : ${ k >= 10 } <br>
m : ${ m } <br>
!m : ${ !m } <br>

</body>
</html>