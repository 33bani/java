<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<% 
request.setCharacterEncoding("UTF-8");
String name = request.getParameter("name");
String number = request.getParameter("number");
String email = request.getParameter("email");

String route = request.getParameter("route");
String sports = request.getParameter("sports");
String contact = request.getParameter("contact");
%>
<table>
	<tr>
		<th> 이름  |</th>
		<th> 전화번호  |</th>
		<th> 이메일  |</th>
		<th> 알게된 경로  |</th>
		<th> 좋아하는 스포츠  |</th>
		<th> 연락할 방법  </th>
	</tr>
	<tr>
		<th><%out.print(name);%></th>
		<th><%out.print(number);%></th>
		<th><%out.print(email);%></th>
		<th><%out.print(route);%></th>
		<th><%out.print(sports);%></th>
		<th><%out.print(contact);%></th>
	</tr>
</table>
</body>
</html>